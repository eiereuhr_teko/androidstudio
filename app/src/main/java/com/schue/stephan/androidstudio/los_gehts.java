package com.schue.stephan.androidstudio;

import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

public class los_gehts extends AppCompatActivity {

    private Button iBGo;
    private Button iBGoBack;
    private EditText Timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_los_gehts);

        iBGo = (Button) findViewById(R.id.iBGo);
        iBGoBack = (Button) findViewById(R.id.iBGoBack);
        Timer = (EditText) findViewById(R.id.Timer);


        iBGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CountDownTimer countdowntimer = new CountDownTimer(8000,1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                        long minutes = millisUntilFinished / 1000 / 60;
                        long seconds = (millisUntilFinished / 1000 ) - minutes * 60;
                        Timer.setText("0" +minutes+ ":" + seconds);
                    }

                    @Override
                    public void onFinish() {
                        try {
                            Thread.sleep(2000);                 //1000 milliseconds ist ei sekunde.
                        } catch(InterruptedException ex) {
                            Thread.currentThread().interrupt();
                        }
                        Intent intent = new Intent(getBaseContext(),MainActivity.class);
                        startActivity(intent);
                    }
                }.start();

            }
        });

        iBGoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(),MainActivity.class);
                Timer.setText("random");
                startActivity(intent);



            }
        });
    }
}
