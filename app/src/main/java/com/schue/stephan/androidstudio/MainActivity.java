package com.schue.stephan.androidstudio;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    private ImageButton iB50g;
    private ImageButton iB60g;
    private ImageButton iB63g;

    private EditText Timer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iB50g = (ImageButton) findViewById(R.id.iB50g);
        iB60g = (ImageButton) findViewById(R.id.iB60g);
        iB63g = (ImageButton) findViewById(R.id.iB63g);

        Timer = (EditText) findViewById(R.id.Timer);

        iB50g.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(),los_gehts.class);
                Timer.setText("random");
                startActivity(intent);
            }
        });

        iB60g.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(),los_gehts.class);
                Timer.setText("random");
                startActivity(intent);

            }
        });

        iB63g.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(),los_gehts.class);
                Timer.setText("random");
                startActivity(intent);
            }
        });



    }
}
